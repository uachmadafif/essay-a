<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php 
		function hitung($angka1, $angka2)
		{
				$a= $angka1;
				$b= $angka2;
				$hasil= $a*$b;
				return $hasil;
		}

		$hasil=hitung(100,2);
		echo "Perkalian 100 x 2 adalah $hasil";
		echo "<br />";
		echo "Perkalian 7 x 2 adalah ".hitung(7,2);

		function pertambahan($angka1, $angka2)
		{
				$a= $angka1;
				$b= $angka2;
				$hasil= $a+$b;
				return $hasil;
		}

		echo("<br>");

		$hasil=pertambahan(2,3);
		echo "Pertambahan 2 + 3 adalah $hasil";
		echo "<br />";
		echo "Pertambahan 10 + 2 adalah ".pertambahan(10,2);

		function pembagian($angka1, $angka2)
		{
				$a= $angka1;
				$b= $angka2;
				$hasil= $a/$b;
				return $hasil;
		}

		echo("<br>");

		$hasil=pembagian(100,25);
		echo "Pembagian 100 / 25 adalah $hasil";
		echo "<br />";
		echo "Pembagian 50 / 25 adalah ".pembagian(50,25);

		function sisabagi($angka1, $angka2)
		{
				$a= $angka1;
				$b= $angka2;
				$hasil= $a%$b;
				return $hasil;
		}

		echo("<br>");

		$hasil=sisabagi(10,2);
		echo "sisabagi 10 % 2 adalah $hasil";
		echo "<br />";
		echo "sisabagi 20 % 2 adalah ".sisabagi(20,2);

		function kurang($angka1, $angka2)
		{
				$a= $angka1;
				$b= $angka2;
				$hasil= $a-$b;
				return $hasil;
		}

		echo("<br>");

		$hasil=kurang(99,2);
		echo "kurang 99 - 2 adalah $hasil";
		echo "<br />";
		echo "kurang 200 - 20 adalah ".kurang(200,20);
	 ?>
</body>
</html>